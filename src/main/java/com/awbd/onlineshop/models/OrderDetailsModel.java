package com.awbd.onlineshop.models;

import lombok.Data;

@Data
public class OrderDetailsModel {
    private Long productId;
    private int units;
}
