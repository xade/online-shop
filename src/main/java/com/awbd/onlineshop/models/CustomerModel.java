package com.awbd.onlineshop.models;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CustomerModel {
    @NotBlank(message = "Last Name can't be empty!")
    @NotNull(message = "Last Name can't be null!")
    private String lastName;
    @NotBlank(message = "First Name can't be empty!")
    @NotNull(message = "First Name can't be null!")
    private String firstName;
    @NotBlank(message = "Address can't be empty!")
    @NotNull(message = "Address can't be null!")
    private String address;
    @NotBlank(message = "Phone Number can't be null!")
    @NotNull(message = "Phone Number can't be null!")
    private String phoneNumber;
    @NotBlank(message = "Email can't be empty!")
    @NotNull(message = "Email can't be null!")
    private String email;

    private List<Long> orders;
}
