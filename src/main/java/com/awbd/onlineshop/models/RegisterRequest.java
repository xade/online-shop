package com.awbd.onlineshop.models;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterRequest {
    @NotBlank(message = "Username can't be empty!")
    @NotNull(message = "Username can't be null!")
    @Size(min = 3, max = 15, message = "Username has to be at least 3 chars and maximum 15 chars")
    private String username;
    @NotBlank(message = "Password can't be empty!")
    @NotNull(message = "Password can't be null!")
    @Size(min = 3, max = 15, message = "Password has to be at least 3 chars and maximum 15 chars")
    private String password;
}
