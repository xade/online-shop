package com.awbd.onlineshop.models.Mappers;

import com.awbd.onlineshop.dtos.Customer;
import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.OrderDetails;
import com.awbd.onlineshop.dtos.Shipper;
import com.awbd.onlineshop.models.OrderModel;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper {

    private OrderDetailsMapper orderDetailsMapper = new OrderDetailsMapper();

    public Order orderModelToOrder(OrderModel orderModel, Customer customer, List<OrderDetails> orderDetails, Shipper shipper) {
        Order order = new Order();
        order.setOrderStatus(orderModel.getOrderStatus());
        order.setCustomer(customer);
        order.setOrderDetails(orderDetails);
//        order.setDateDelivered(new Date(orderModel.getDateDelivered()));
//        order.setDatePlaced(new Date(orderModel.getDatePlaced()));
        order.setDatePlaced(new Date());
        order.setDateDelivered(new Date());
        order.setShipper(shipper);
        double price = orderDetails.stream().mapToDouble(orderDet -> orderDet.getProduct().getPricePerUnit() * orderDet.getUnitsOfProduct()).sum();
        order.setTotalPrice(price);
        return order;
    }

    public OrderModel orderToOrderModel(Order order) {
        OrderModel orderModel = new OrderModel();
        orderModel.setOrderDetails(order.getOrderDetails().stream().map(orderDetails -> {
            return orderDetailsMapper.orderDetailsToOrderDetailsModel(orderDetails);
        }).collect(Collectors.toList()));
        orderModel.setCustomerId(order.getCustomer().getId());
        orderModel.setOrderStatus(order.getOrderStatus());
        orderModel.setDateDelivered(order.getDateDelivered().toString());
        orderModel.setDatePlaced(order.getDatePlaced().toString());
        orderModel.setTotalPrice(order.getTotalPrice());
        orderModel.setShipperId(order.getShipper().getId());

        return orderModel;
    }
}
