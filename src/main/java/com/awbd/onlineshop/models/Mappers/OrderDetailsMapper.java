package com.awbd.onlineshop.models.Mappers;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.OrderDetails;
import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.models.OrderDetailsModel;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailsMapper {

    public OrderDetails orderDetailsModelToOrderDetails(OrderDetailsModel orderDetailsModel, Product product, Order order) {
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setOrder(order);
        orderDetails.setProduct(product);
        orderDetails.setUnitsOfProduct(orderDetailsModel.getUnits());
        return orderDetails;
    }

    public OrderDetailsModel orderDetailsToOrderDetailsModel(OrderDetails orderDetails) {
        OrderDetailsModel orderDetailsModel = new OrderDetailsModel();
        orderDetailsModel.setUnits(orderDetails.getUnitsOfProduct());
        orderDetailsModel.setProductId(orderDetails.getProduct().getId());
        return orderDetailsModel;
    }
}
