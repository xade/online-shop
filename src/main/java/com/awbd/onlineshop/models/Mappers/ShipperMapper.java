package com.awbd.onlineshop.models.Mappers;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.Shipper;
import com.awbd.onlineshop.models.ShipperModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ShipperMapper {

    public Shipper shipperModelToShipper(ShipperModel shipperModel, List<Order> orders) {
        Shipper shipper = new Shipper();
        shipper.setName(shipperModel.getName());
        shipper.setPhoneNumber(shipperModel.getPhoneNumber());
        shipper.setOrders(orders);
        return shipper;
    }
}
