package com.awbd.onlineshop.models.Mappers;

import com.awbd.onlineshop.models.ProductModel;
import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.dtos.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {
     public Product productModelToProduct(ProductModel productModel, List<Category> categories) {
        Product product = new Product();
        product.setName(productModel.getName());
        product.setPricePerUnit(productModel.getPricePerUnit());
        product.setUnitsInStock(productModel.getUnitsInStock());
        product.setInfo(productModel.getInfo());
        product.setCategories(categories);

        return product;
    }
}

