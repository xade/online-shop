package com.awbd.onlineshop.models.Mappers;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.Customer;
import com.awbd.onlineshop.models.CustomerModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerMapper {

    public Customer customerModelToCustomer(CustomerModel customerModel, List<Order> orders) {
        Customer customer = new Customer();
        customer.setAddress(customerModel.getAddress());
        customer.setEmail(customerModel.getEmail());
        customer.setFirstName(customerModel.getFirstName());
        customer.setLastName(customerModel.getLastName());
        customer.setPhoneNumber(customerModel.getPhoneNumber());
        customer.setOrders(orders);
        return customer;
    }
}
