package com.awbd.onlineshop.models;
import com.awbd.onlineshop.dtos.ProductInfo;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProductModel {
    @NotBlank(message = "Name can't be empty!")
    @NotNull(message = "Name can't be null!")
    private String name;
    @Min(value = 0, message = "Price can't be lower than 0!")
    @NotNull(message = "Price can't be null!")
    private double pricePerUnit;
    @Min(value = 0, message = "The units in the stock can't be lower than 0!")
    @NotNull(message = "The units in the stock can't be null!")
    private int unitsInStock;

    @NotNull(message = "Product info can't be null!")
    private ProductInfo info;
    @NotNull(message = "Product info can't be null!")
    private List<Long> categories;
}
