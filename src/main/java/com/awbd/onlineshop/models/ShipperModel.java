package com.awbd.onlineshop.models;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ShipperModel {
    @NotBlank(message = "Name can't be empty!")
    @NotNull(message = "Name can't be null!")
    private String name;
    @NotBlank(message = "Phone number can't be empty!")
    @NotNull(message = "Phone number can't be null!")
    private String phoneNumber;

    private List<Long> orders;

}
