package com.awbd.onlineshop.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AuthenticationRequest {

    @NotNull(message = "Username can't be null!")
    @NotEmpty(message = "Username can't be empty!")
    private String username;

    @NotNull(message = "Password can't be null!")
    @NotEmpty(message = "Password can't be empty!")
    private String password;

    public AuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
