package com.awbd.onlineshop.models;

import com.awbd.onlineshop.dtos.OrderStatus;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class OrderModel {
    @NotBlank(message = "Date Placed can't be empty!")
    @NotNull(message = "Date Placed can't be null!")
    private String datePlaced;
    private String dateDelivered;
    private double totalPrice;
    @NotNull(message = "Order status can't be null!")
    private OrderStatus orderStatus;
    @NotNull(message = "Order details can't be null!")
    private List<OrderDetailsModel> orderDetails;
    @Min(value = 1, message = "Customer Id can't be lower than 0!")
    @NotNull(message = "Customer ID can't be null!")
    private Long customerId;
    @Min(value = 1, message = "Shipper ID can't be lower than 0!")
    @NotNull(message = "Shipper ID can't be null!")
    private Long shipperId;
}
