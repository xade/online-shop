package com.awbd.onlineshop.globalErrorHandle;

public class ObjectNotFoundException extends  RuntimeException {

    public ObjectNotFoundException(String message) {
        super(message);
    }
}
