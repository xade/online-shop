package com.awbd.onlineshop.dtos;

public enum OrderStatus {
    PLACED, DONE, PACKING, IN_DELIVERY
}
