package com.awbd.onlineshop.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "orders"})
public class Shipper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String phoneNumber;

    @OneToMany(mappedBy = "shipper",
            fetch = FetchType.LAZY)
    private List<Order> orders;
}
