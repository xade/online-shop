package com.awbd.onlineshop.dtos;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Enumerated(value = EnumType.STRING)
    AllRoles name;

    public Role() {

    }

    public Role(AllRoles name) {
        this.name = name;
    }
}
