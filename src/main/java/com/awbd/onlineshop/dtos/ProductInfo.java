package com.awbd.onlineshop.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "product"})
public class ProductInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 100000)
    private Byte[] image;
    private String description;

    @OneToOne(mappedBy = "info", fetch = FetchType.LAZY, orphanRemoval = true)
    private Product product;
}
