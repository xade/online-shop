package com.awbd.onlineshop.dtos;

public enum AllRoles {
    ROLE_USER,
    ROLE_ADMIN
}
