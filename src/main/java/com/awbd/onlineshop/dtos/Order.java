package com.awbd.onlineshop.dtos;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "db_order")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date datePlaced;
    private Date dateDelivered;
    private double totalPrice;

    @Enumerated(value = EnumType.STRING)
    private OrderStatus orderStatus;

    @OneToMany(mappedBy = "order")
    private List<OrderDetails> orderDetails;

    @ManyToOne
    @JoinTable(name = "customer_orders",
        joinColumns = @JoinColumn(name = "db_order_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id"))
    private Customer customer;

    @ManyToOne
    @JoinTable(name = "shipper_orders",
            joinColumns = @JoinColumn(name = "db_order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "shipper_id", referencedColumnName = "id"))
    private Shipper shipper;

    public void SetThisAsOrder() {
        this.orderDetails = this.orderDetails.stream().map(ord -> {
            ord.setOrder(this);
            return ord;
        }).collect(Collectors.toList());
    }
}
