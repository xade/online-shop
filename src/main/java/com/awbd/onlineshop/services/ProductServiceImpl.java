package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.repositories.ProductRepository;
import com.awbd.onlineshop.services.interfaces.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl (ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        List<Product> products = productRepository.findAll();
        logger.info("S-au afisat produsele {}", products);
        return products;
    }

    @Override
    public Product findById(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (!productOptional.isPresent()) {
            throw new RuntimeException("Product not found!");
        }
        logger.info("S-a afisat produsul cu id-ul " + id + " {}", productOptional.get());
        return productOptional.get();
    }

    @Override
    public Product add(Product product) {
        Product savedProduct = productRepository.save(product);
        logger.info("S-a adaugat produsul {}", savedProduct);
        return savedProduct;
    }

    @Override
    public Product update(Product newProduct) {
        if (!productRepository.findById(newProduct.getId()).isPresent()) {
            throw new RuntimeException("Product not found!");
        }
        Product updated = productRepository.save(newProduct);
        logger.info("S-a updatat produsul {}", updated);
        return updated;
    }

    @Override
    public void deleteById(Long id) {
        logger.info("S-a sters produsul cu id-ul {}",id);
        productRepository.deleteById(id);
    }
}
