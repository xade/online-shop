package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.repositories.OrderRepository;
import com.awbd.onlineshop.services.interfaces.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);
    OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl (OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> findAll() {
        List<Order> orders = orderRepository.findAll();
        logger.info("S-au afisat comenzile {}", orders);
        return orders;
    }

    @Override
    public Order findById(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (!orderOptional.isPresent()) {
            throw new RuntimeException("Order not found!");
        }
        logger.info("S-a afisat comanda cu id-ul " + id + " {}", orderOptional.get());
        return orderOptional.get();
    }

    @Override
    public Order add(Order order) {
        Order savedOrder = orderRepository.save(order);
        logger.info("S-a adaugat coamnda {}", savedOrder);
        return savedOrder;
    }

    @Override
    public Order update(Order newOrder) {
        if (!orderRepository.findById(newOrder.getId()).isPresent()) {
            throw new RuntimeException("Order not found!");
        }
        Order updated = orderRepository.save(newOrder);
        logger.info("S-a updatat comanda {}", updated);
        return updated;
    }

    @Override
    public void deleteById(Long id) {
        logger.info("S-a sters coamnda cu id-ul {}", id);
        orderRepository.deleteById(id);
    }
}
