package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.dtos.ProductInfo;
import com.awbd.onlineshop.repositories.ProductRepository;
import com.awbd.onlineshop.services.interfaces.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {
    private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);
    ProductRepository productRepository;

    @Autowired
    public ImageServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    @Transactional
    public void saveImageFile(Long productId, MultipartFile file) {
        try {
            Product product = productRepository.findById(productId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()) {
                byteObjects[i++] = b;
            }

            ProductInfo info = product.getInfo();
            if (info == null) {
                info = new ProductInfo();
            }

            info.setImage(byteObjects);
            product.setInfo(info);
            info.setProduct(product);
            Product saved = productRepository.save(product);
            logger.info("S-a adaugat produsul {}", saved);
        }
        catch (IOException e) {

        }
    }
}
