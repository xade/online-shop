package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.repositories.CategoryRepository;
import com.awbd.onlineshop.services.interfaces.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl (CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        List<Category> categories = categoryRepository.findAll();
        logger.info("S-au afisat categoriile {}", categories);
        return categories;
    }

    @Override
    public Category findById(Long id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (!categoryOptional.isPresent()) {
            throw new RuntimeException("Category not found!");
        }
        logger.info("S-a afisat categoria cu id-ul " + id + " {}", categoryOptional.get());
        return categoryOptional.get();
    }

    @Override
    public Category add(Category category) {
        Category savedCategory = categoryRepository.save(category);
        logger.info("S-a adaugat categoria {}", savedCategory);
        return savedCategory;
    }

    @Override
    public Category update(Category newCategory) {
        if (!categoryRepository.findById(newCategory.getId()).isPresent()) {
            throw new RuntimeException("Category not found!");
        }
        Category updated = categoryRepository.save(newCategory);
        logger.info("S-a updatat categoria {}", updated);
        return updated;
    }

    @Override
    public void deleteById(Long id) {
        logger.info("S-a sters categoria cu id-ul {}",id);
        categoryRepository.deleteById(id);
    }
}
