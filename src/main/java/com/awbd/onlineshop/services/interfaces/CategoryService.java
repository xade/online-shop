package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findById(Long id);
    Category add(Category category);
    Category update(Category newCategory);
    void deleteById(Long id);
}
