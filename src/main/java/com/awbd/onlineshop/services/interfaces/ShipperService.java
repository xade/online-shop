package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.Shipper;

import java.util.List;

public interface ShipperService {
    List<Shipper> findAll();
    Shipper findById(Long id);
    Shipper add(Shipper shipper);
    Shipper update(Shipper newShipper);
    void deleteById(Long id);
}
