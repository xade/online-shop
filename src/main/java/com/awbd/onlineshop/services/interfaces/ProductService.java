package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    Product findById(Long id);
    Product add(Product product);
    Product update(Product newProduct);
    void deleteById(Long id);
}
