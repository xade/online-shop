package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.Order;

import java.util.List;

public interface OrderService {
    List<Order> findAll();
    Order findById(Long id);
    Order add(Order order);
    Order update(Order newOrder);
    void deleteById(Long id);
}
