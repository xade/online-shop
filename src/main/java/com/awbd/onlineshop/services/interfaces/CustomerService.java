package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll();
    Customer findById(Long id);
    Customer add(Customer customer);
    Customer update(Customer newCustomer);
    void deleteById(Long id);
}
