package com.awbd.onlineshop.services.interfaces;

import com.awbd.onlineshop.dtos.ProductInfo;

public interface ProductInfoService {
    ProductInfo findById(Long id);
    ProductInfo add(ProductInfo newProductInfo);
    ProductInfo update(ProductInfo productInfo);
}
