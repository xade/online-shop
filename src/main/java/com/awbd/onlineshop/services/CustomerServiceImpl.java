package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Customer;
import com.awbd.onlineshop.repositories.CustomerRepository;
import com.awbd.onlineshop.services.interfaces.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl (CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = customerRepository.findAll();
        logger.info("S-au afisat clientii {}", customers);
        return customers;
    }

    @Override
    public Customer findById(Long id) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (!customerOptional.isPresent()) {
            throw new RuntimeException("Customer not found!");
        }
        logger.info("S-a afisat clientul cu id-ul " + id + " {}", customerOptional.get());
        return customerOptional.get();
    }

    @Override
    public Customer add(Customer customer) {
        Customer savedCustomer = customerRepository.save(customer);
        logger.info("S-a adaugat clientul {}", savedCustomer);
        return savedCustomer;
    }

    @Override
    public Customer update(Customer newCustomer) {
        if (!customerRepository.findById(newCustomer.getId()).isPresent()) {
            throw new RuntimeException("Customer not found!");
        }
        Customer updated = customerRepository.save(newCustomer);
        logger.info("S-a updatat clientul {}", updated);
        return customerRepository.save(newCustomer);
    }

    @Override
    public void deleteById(Long id) {
        logger.info("S-a sters clientul cu id-ul {}",id);
        customerRepository.deleteById(id);
    }
}
