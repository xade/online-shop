package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.dtos.ProductInfo;
import com.awbd.onlineshop.repositories.ProductInfoRepository;
import com.awbd.onlineshop.services.interfaces.ProductInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {
    private static final Logger logger = LoggerFactory.getLogger(ProductInfoServiceImpl.class);

    ProductInfoRepository productInfoRepository;

    @Autowired
    public ProductInfoServiceImpl (ProductInfoRepository productInfoRepository) {
        this.productInfoRepository = productInfoRepository;
    }

    @Override
    public ProductInfo findById(Long id) {
        Optional<ProductInfo> productInfoOptional = productInfoRepository.findById(id);
        if (!productInfoOptional.isPresent()) {
            throw new RuntimeException("Product info not found!");
        }
        logger.info("S-a afisat info cu id-ul " + id + " {}", productInfoOptional.get());
        return productInfoOptional.get();
    }

    @Override
    public ProductInfo add(ProductInfo productInfo) {
        ProductInfo savedProductInfo = productInfoRepository.save(productInfo);
        logger.info("S-a adaugat info {}", savedProductInfo);
        return savedProductInfo;
    }

    @Override
    public ProductInfo update(ProductInfo newProductInfo) {
        if (!productInfoRepository.findById(newProductInfo.getId()).isPresent()) {
            throw new RuntimeException("Product info not found!");
        }
        ProductInfo updated = productInfoRepository.save(newProductInfo);
        logger.info("S-a updatat info {}", updated);
        return updated;
    }
}
