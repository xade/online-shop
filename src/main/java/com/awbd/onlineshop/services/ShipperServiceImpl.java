package com.awbd.onlineshop.services;

import com.awbd.onlineshop.dtos.Shipper;
import com.awbd.onlineshop.repositories.ShipperRepository;
import com.awbd.onlineshop.services.interfaces.ShipperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShipperServiceImpl implements ShipperService {
    private static final Logger logger = LoggerFactory.getLogger(ShipperServiceImpl.class);
    ShipperRepository shipperRepository;

    @Autowired
    public ShipperServiceImpl (ShipperRepository shipperRepository) {
        this.shipperRepository = shipperRepository;
    }

    @Override
    public List<Shipper> findAll() {
        List<Shipper> shippers = shipperRepository.findAll();
        logger.info("S-au afisat expediatorii {}", shippers);
        return shippers;
    }

    @Override
    public Shipper findById(Long id) {
        Optional<Shipper> shipperOptional = shipperRepository.findById(id);
        if (!shipperOptional.isPresent()) {
            throw new RuntimeException("Shipper not found!");
        }
        logger.info("S-a afisat expediatorul cu id-ul " + id + " {}", shipperOptional.get());
        return shipperOptional.get();
    }

    @Override
    public Shipper add(Shipper shipper) {
        Shipper savedShipper = shipperRepository.save(shipper);
        logger.info("S-a adaugat expediatorul {}", savedShipper);
        return savedShipper;
    }

    @Override
    public Shipper update(Shipper newShipper) {
        if (!shipperRepository.findById(newShipper.getId()).isPresent()) {
            throw new RuntimeException("Shipper not found!");
        }
        Shipper updated = shipperRepository.save(newShipper);
        logger.info("S-a updatat expediatorul {}", updated);
        return updated;
    }

    @Override
    public void deleteById(Long id) {
        logger.info("S-a sters categoria cu id-ul {}", id);
        shipperRepository.deleteById(id);
    }
}
