package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.Customer;
import com.awbd.onlineshop.models.CustomerModel;
import com.awbd.onlineshop.models.Mappers.CustomerMapper;
import com.awbd.onlineshop.services.interfaces.CustomerService;
import com.awbd.onlineshop.services.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    CustomerService customerService;
    OrderService orderService;
    CustomerMapper customerMapper;

    @Autowired
    public CustomerController(CustomerService customerService, OrderService orderService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.orderService = orderService;
        this.customerMapper = customerMapper;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Customer>> customerList() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> customerById(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.findById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Customer> addCustomer(@RequestBody @Valid CustomerModel newCustomer) {
        return ResponseEntity.ok(customerService.add(customerMapper.customerModelToCustomer(newCustomer, null)));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Customer> updateCustomer(@RequestBody @Valid CustomerModel newCustomer, @PathVariable Long id) {
        Customer customer = customerMapper.customerModelToCustomer(newCustomer, findOrders(newCustomer.getOrders()));
        customer.setId(id);
        return ResponseEntity.ok(customerService.update(customer));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Long id) {
        customerService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private List<Order> findOrders(List<Long> orderIds) {
        if (orderIds == null || orderIds.isEmpty()) {
            return null;
        }
        return orderIds.stream().map(orderService::findById).collect(Collectors.toList());
    }
}
