package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Category>> categoriesList() {
        return ResponseEntity.ok(categoryService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> categoryById(@PathVariable Long id) {
        return ResponseEntity.ok(categoryService.findById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Category> addCategory(@RequestBody @Valid Category newCategory) {
        return ResponseEntity.ok(categoryService.add(newCategory));
    }

    @PutMapping("/update")
    public ResponseEntity<Category> updateCategory(@RequestBody @Valid Category newCategory) {
        return ResponseEntity.ok(categoryService.update(newCategory));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCategory(@PathVariable Long id) {
        categoryService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
