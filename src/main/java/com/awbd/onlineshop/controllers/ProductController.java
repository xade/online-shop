package com.awbd.onlineshop.controllers;


import com.awbd.onlineshop.dtos.ProductInfo;
import com.awbd.onlineshop.models.Mappers.ProductMapper;
import com.awbd.onlineshop.models.ProductModel;
import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.services.interfaces.CategoryService;
import com.awbd.onlineshop.services.interfaces.ImageService;
import com.awbd.onlineshop.services.interfaces.ProductInfoService;
import com.awbd.onlineshop.services.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    ProductService productService;
    CategoryService categoryService;
    ProductInfoService productInfoService;
    ImageService imageService;
    ProductMapper productMapper;

    @Autowired
    public ProductController(ProductService productService, CategoryService categoryService, ProductMapper productMapper, ImageService imageService, ProductInfoService productInfoService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.imageService = imageService;
        this.productMapper = productMapper;
        this.productInfoService = productInfoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Product>> productsList() {
        return ResponseEntity.ok(productService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> productById(@PathVariable Long id) {
        return ResponseEntity.ok(productService.findById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Product> addProduct(@RequestBody @Valid ProductModel newProductModel) {
        List<Category> categories = findCategories(newProductModel.getCategories());
        Product product = productService.add(productMapper.productModelToProduct(newProductModel, categories));
        return ResponseEntity.ok(product);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Product> updateProduct(@RequestBody @Valid ProductModel newProduct, @PathVariable Long id) {
        List<Category> categories = findCategories(newProduct.getCategories());
        Product prod = productService.findById(id);
        ProductInfo oldInfo = productInfoService.findById(prod.getInfo().getId());
        oldInfo.setDescription(newProduct.getInfo().getDescription());
        oldInfo.setImage(newProduct.getInfo().getImage() != null ? newProduct.getInfo().getImage() : oldInfo.getImage());
        prod.setInfo(oldInfo);
        prod.setCategories(categories);
        prod.setPricePerUnit(newProduct.getPricePerUnit());
        prod.setName(newProduct.getName());
        prod.setUnitsInStock(newProduct.getUnitsInStock());
        return ResponseEntity.ok(productService.update(prod));
    }

    @PutMapping("/update/image/{id}")
    public ResponseEntity updateProductImage(@RequestParam("imageFile") MultipartFile file, @PathVariable Long id) {
        imageService.saveImageFile(id, file);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        productService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    // utils
    private List<Category> findCategories(List<Long> categoryIds) {
        if (categoryIds == null || categoryIds.isEmpty()) {
            return null;
        }
        return categoryIds.stream().map(categoryService::findById).collect(Collectors.toList());
    }
}
