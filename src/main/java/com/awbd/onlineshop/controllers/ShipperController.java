package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.Order;
import com.awbd.onlineshop.dtos.Shipper;
import com.awbd.onlineshop.models.Mappers.ShipperMapper;
import com.awbd.onlineshop.models.ShipperModel;
import com.awbd.onlineshop.services.interfaces.OrderService;
import com.awbd.onlineshop.services.interfaces.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/shipper")
public class ShipperController {

    ShipperService shipperService;
    OrderService orderService;
    ShipperMapper shipperMapper;

    @Autowired
    public ShipperController(ShipperService shipperService, OrderService orderService, ShipperMapper shipperMapper) {

        this.shipperService = shipperService;
        this.orderService = orderService;
        this.shipperMapper = shipperMapper;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Shipper>> shipperList() {
        return ResponseEntity.ok(shipperService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Shipper> shipperById(@PathVariable Long id) {
        return ResponseEntity.ok(shipperService.findById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Shipper> addShipper(@RequestBody @Valid ShipperModel newShipper) {
        Shipper shipper = shipperMapper.shipperModelToShipper(newShipper, null);
        return ResponseEntity.ok(shipperService.add(shipper));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Shipper> updateShipper(@RequestBody @Valid ShipperModel newShipper, @PathVariable Long id) {
        Shipper shipper = shipperMapper.shipperModelToShipper(newShipper, getOrders(newShipper.getOrders()));
        shipper.setId(id);
        return ResponseEntity.ok(shipperService.update(shipper));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteShipper(@PathVariable Long id) {
        shipperService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private List<Order> getOrders(List<Long> orderIds) {
        if (orderIds == null || orderIds.isEmpty()) {
            return null; // or empty list idk if it matters
        }
        return orderIds.stream().map(orderService::findById).collect(Collectors.toList());
    }
}
