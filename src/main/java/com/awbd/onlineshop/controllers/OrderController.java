package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.*;
import com.awbd.onlineshop.models.Mappers.OrderDetailsMapper;
import com.awbd.onlineshop.models.Mappers.OrderMapper;
import com.awbd.onlineshop.models.OrderDetailsModel;
import com.awbd.onlineshop.models.OrderModel;
import com.awbd.onlineshop.services.interfaces.CustomerService;
import com.awbd.onlineshop.services.interfaces.OrderService;
import com.awbd.onlineshop.services.interfaces.ProductService;
import com.awbd.onlineshop.services.interfaces.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    OrderService orderService;
    CustomerService customerService;
    ShipperService shipperService;
    ProductService productService;

    OrderMapper orderMapper;
    OrderDetailsMapper orderDetailsMapper;

    @Autowired
    public OrderController(OrderService orderService, CustomerService customerService, ShipperService shipperService, ProductService productService, OrderMapper orderMapper, OrderDetailsMapper orderDetailsMapper) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.shipperService = shipperService;
        this.productService = productService;

        this.orderMapper = orderMapper;
        this.orderDetailsMapper = orderDetailsMapper;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Order>> ordersList() {
        return ResponseEntity.ok(orderService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> orderById(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

//    @PostMapping("/add")
//    public ResponseEntity<Order> addOrder(@RequestBody @Valid OrderModel newOrder) {
//        Customer customer = customerService.findById(newOrder.getCustomerId());
//        Shipper shipper = shipperService.findById(newOrder.getShipperId());
//        Order order = orderMapper.orderModelToOrder(newOrder, customer, getOrderDetails(newOrder.getOrderDetails()), shipper);
//        Order asda = orderService.add(order);
//        return ResponseEntity.ok(asda);
//    }
//
//    @PutMapping("/update/{id}")
//    public ResponseEntity<Order> updateOrder(@RequestBody @Valid OrderModel newOrder, @PathVariable Long id) {
//        Customer customer = customerService.findById(newOrder.getCustomerId());
//        Shipper shipper = shipperService.findById(newOrder.getShipperId());
//        Order order = orderMapper.orderModelToOrder(newOrder, customer, getOrderDetails(newOrder.getOrderDetails()), shipper);
//        order.setId(id);
//        return ResponseEntity.ok(orderService.update(order));
//    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteOrder(@PathVariable Long id) {
        orderService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    List<OrderDetails> getOrderDetails(List<OrderDetailsModel> orderDetailsModels) {
        if (orderDetailsModels == null || orderDetailsModels.isEmpty()) {
            return null;
        }
        return orderDetailsModels.stream().map(ordDet -> {
            Product product = productService.findById(ordDet.getProductId());
            OrderDetails ord = orderDetailsMapper.orderDetailsModelToOrderDetails(ordDet, product, null);
            return ord;
        }).collect(Collectors.toList());
    }
}
