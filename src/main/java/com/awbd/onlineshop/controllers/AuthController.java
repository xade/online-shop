package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.AllRoles;
import com.awbd.onlineshop.dtos.Role;
import com.awbd.onlineshop.dtos.User;
import com.awbd.onlineshop.models.AuthenticationRequest;
import com.awbd.onlineshop.models.AuthenticationResponse;
import com.awbd.onlineshop.models.RegisterRequest;
import com.awbd.onlineshop.repositories.RoleRepository;
import com.awbd.onlineshop.repositories.UserRepository;
import com.awbd.onlineshop.services.JwtUtil;
import com.awbd.onlineshop.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.RowId;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/signin")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody @Valid AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity registerUser(@RequestBody @Valid RegisterRequest registerRequest) {
        if (userRepository.existsByUsername(registerRequest.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exists!");
        }

        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        user.setEnabled(true);

        Optional<Role> userRole = roleRepository.findByName(AllRoles.ROLE_USER);
        if (!userRole.isPresent()) {
            throw new RuntimeException("Role not defined");
        }

        Set<Role> roles = new HashSet<>();
        roles.add(userRole.get());
        user.setRoles(roles);

        User addedUser = userRepository.save(user);

        return new ResponseEntity(HttpStatus.OK);
    }
}
