package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Order;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
    List<Order> findAll();
//    List<Order> findByDatePlacedAfter(Date date);
//    List<Order> findByDatePlacedBefore(Date date);
//    List<Order> findByDatePlaced(Date date);
//
//    List<Order> findByDateDeliveredAfter(Date date);
//    List<Order> findByDateDeliveredBefore(Date date);
//    List<Order> findByDateDelivered(Date date);
//
//    List<Order> findByTotalPrice(double price);
//
//    List<Order> findByOrderStatus(OrderStatus status);
//
//    List<Order> findByCustomer(Customer customer);
//
//    List<Order> findByShipper(Shipper shipper);
}
