package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Shipper;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShipperRepository extends CrudRepository<Shipper, Long> {
    List<Shipper> findAll();
}
