package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    List<Category> findAll();
    List<Category> findAllByNameLike(String name);
}
