package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.dtos.ProductInfo;
import org.springframework.data.repository.CrudRepository;

public interface ProductInfoRepository extends CrudRepository<ProductInfo, Long> {
    ProductInfo findByProduct(Product product);
}
