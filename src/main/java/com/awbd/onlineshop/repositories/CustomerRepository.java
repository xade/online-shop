package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
    List<Customer> findAll();
}
