package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    List<Product> findAll();
//    List<Product> findByNameLike(String name);
//    List<Product> findByPricePerUnitGreaterThanEqual(double price);
//    List<Product> findByPricePerUnitLessThanEqual(double price);
//
//    List<Product> findByUnitsInStockGreaterThanEqual(int units);
//    List<Product> findByUnitsInStockLessThanEqual(int units);
}
