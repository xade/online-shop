update product set info = null;

delete from customer_orders;
delete from shipper_orders;
delete from order_details;
delete from shipper;
delete from customer;
delete from product_category;
delete from category;
delete from product_info;
delete from product;
delete from db_order;
delete from roles;

alter table category AUTO_INCREMENT = 1;
alter table product_info AUTO_INCREMENT = 1;
alter table product AUTO_INCREMENT = 1;
alter table db_order AUTO_INCREMENT = 1;
alter table customer AUTO_INCREMENT = 1;
alter table shipper AUTO_INCREMENT = 1;
alter table roles AUTO_INCREMENT = 1;
--
INSERT INTO roles(name) VALUES ('ROLE_USER');
INSERT INTO roles(name) VALUES ('ROLE_ADMIN');
-- --
-- insert into category(name) values ('electronice');
-- insert into category(name) values ('gaming');
-- insert into category(name) values ('haine');
-- insert into category(name) values ('supermarket');
-- insert into category(name) values ('activitati sportive');
-- insert into category(name) values ('carti');
-- insert into category(name) values ('relaxare');
--
-- insert into product_info(description, image) values ("O carte de citit in serile ploioase.", null);
-- insert into product_info(description, image) values ("O carte de citit de bucurie", null);
-- insert into product(name, price_per_unit, units_in_stock, info) values ("Shogunul", 10, 20, 1);
-- insert into product(name, price_per_unit, units_in_stock, info) values ("Shogunul2", 22, 12, 2);
--
-- insert into product_category(category_id, product_id) values (6, 1);
-- insert into product_category(category_id, product_id) values (7, 1);
--
-- insert into db_order(date_placed, date_delivered, total_price, order_status) values ("20201212", "20201212", "30", "DONE");
-- insert into customer(address, email, first_name, last_name, phone_number) values ("addr", "email@email.com", "FName", "LName", "123456");
-- insert into shipper(name, phone_number) values ("NameShipper", "123");
--
-- insert into shipper_orders(shipper_id, db_order_id) values (1, 1);
-- insert into customer_orders(customer_id, db_order_id) values (1, 1);
-- insert into order_details(units_of_product, order_id, product_id) values (2, 1, 1);
-- insert into order_details(units_of_product, order_id, product_id) values (3, 1, 2);