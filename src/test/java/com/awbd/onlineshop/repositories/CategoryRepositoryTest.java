package com.awbd.onlineshop.repositories;

import com.awbd.onlineshop.dtos.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("mysql")
@Rollback(false)
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;
    private String categoryName = "TestCategory";

    @Test
    public void addCategory() {
        Category category = new Category();
        category.setId(Long.parseLong("1"));
        category.setName(categoryName);
        categoryRepository.save(category);
    }


    @Test
    public void getCategoryByName() {
        List<Category> categories = categoryRepository.findAllByNameLike("%at%");
        assertFalse(categories.isEmpty());
    }

    @Test
    public void deleteCategory() {
        categoryRepository.deleteById(Long.parseLong("1"));
    }
}
