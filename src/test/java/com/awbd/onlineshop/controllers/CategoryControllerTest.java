package com.awbd.onlineshop.controllers;

import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.repositories.CategoryRepository;
import com.awbd.onlineshop.services.CategoryServiceImpl;
import com.awbd.onlineshop.services.interfaces.CategoryService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CategoryControllerTest {
    @Autowired
    MockMvc mockMvc;

    CategoryService categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Before
    public void setUp() throws Exception {
        categoryService = new CategoryServiceImpl(categoryRepository);
    }

    @Test
    public void getCategoriesHappyFlow() throws Exception {
        Category category = new Category(1L, "Category1");

        when(categoryService.findAll()).thenReturn(
                Arrays.asList(new Category(1L, "Category1"))
        );

//        mockMvc.perform(get("/api/v1/category/all"))
    }
}
