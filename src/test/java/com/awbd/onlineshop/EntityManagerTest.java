package com.awbd.onlineshop;

import com.awbd.onlineshop.dtos.Category;
import com.awbd.onlineshop.dtos.Product;
import com.awbd.onlineshop.dtos.ProductInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("mysql")
@Rollback(false)
public class EntityManagerTest {

    @Autowired
    private EntityManager entityManager;

    @Test
    public void findProduct() {
        Product productFound = entityManager.find(Product.class, 1L);

        assertEquals(productFound.getId(), 1);
    }

    @Test
    public void updateProduct() {
        Product productFound = entityManager.find(Product.class, 1L);
        productFound.setName("Shogun Vol. 1");

        entityManager.persist(productFound);
        entityManager.flush();
    }

    @Test
    public void saveProduct() {
        Category category = new Category();
        category.setName("testCategory");

        ProductInfo info = new ProductInfo();
        info.setDescription("Test description");

        Product product = new Product();
        product.setName("Shogun Vol. 2");
        product.setCategories(List.of(category));
        product.setInfo(info);
        product.setPricePerUnit(2);
        product.setUnitsInStock(15);

        category.setProducts(List.of(product));

        info.setProduct(product);

        entityManager.persist(product);
        entityManager.flush();
        entityManager.clear();
    }
}
